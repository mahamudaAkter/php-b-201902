
<html>
<head>
    <title> Multiple File Upload</title>
    <style>
        #wrapper{ margin: 0 auto;}
        #message{ align : 'center' ; margin-top:100;}
    </style>
</head>
<body>
<div id="wrapper">
<?php
if(isset($_POST['submit'])){

    if(count($_FILES['upload']['name']) > 0) {

        for ($i = 0; $i < count($_FILES['upload']['name']); $i++) {

            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

            if ($tmpFilePath != "") {

                $shortname = $_FILES['upload']['name'][$i];

                $filePath = "uploaded/" . time() . '-' . $_FILES['upload']['name'][$i];

                if (move_uploaded_file($tmpFilePath, $filePath)) {
                    $files[] = $shortname;
                    $listFile = count($files);
                }
            }
        }
    }
   if(isset($listFile)){
        echo "<div id=\"message\" align=\"center\"><h1>".$listFile." File(s) Uploaded Successfully:</h1>";
        echo "<ol type='i'>";
        foreach( $files as $file){
            echo ".<li>$file</li>";
        }
        echo "</ol> </div>";
        unset($files,$listFile);
        }
}
?>
<div id="message" align="center">
<form action="process_file.php" enctype="multipart/form-data" method="post">
    <div>
        <table>
            <tr><td><label for="file"> Add Files :</label></td><td><input id='upload' name="upload[]" type="file" multiple="multiple" /></td></tr>
            <tr><td></td><td><input type="submit" name="submit" value="Submit"></td></tr>
        </table>
    </div>
</form>
    </div>
</div>
</body>
</html>