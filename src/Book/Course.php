<?php
/**
 * Created by PhpStorm.
 * User: MAHAMUDA
 * Date: 4/2/2019
 * Time: 10:57 PM
 */

namespace App;


class Course
{
    private $markBangla;
    private $gradeBangla;

    private $markEnglish;
    private $gradeEnglish;


    private $markMath;
    private $gradeMath;

    public function setMarkBangla($markBangla)
    {

        $this->markBangla = $markBangla;


    }

    public function setGradeBangla()
    {


        $this->markBangla = $this->convertMark2Grade($this->setMarkBangla());
    }






    public function setMarkEnglish($markEnglish)
    {

        $this->markEnglish = $markEnglish;


    }

    public function setGradeEnglish()
    {


        $this->markEnglish = $this->convertMark2Grade($this->setMarkEnglish);
    }




    public function setMarkMath($markMath)
    {

        $this->markMath = $markMath;


    }

    public function setGradeMath()
    {


        $this->markMath = $this->convertMark2Grade($this->setMarkMath);
    }





    public function getMarkBangla()
    {
        return $this->markBangla;
    }

    public function getGradeBangla()
    {
        return $this->gradeBangla;
    }



    public function getMarkEnglish()
    {
        return $this->markEnglish;
    }

    public function getGradeEnglis()
    {
        return $this->gradeEnglish;
    }



    public function getMarkMath()
    {
        return $this->markMath;
    }

    public function getGradeMath()
    {
        return $this->gradeMath;
    }
    public  function convertMark2Grade($mark){


        switch ($mark){
            case($mark>79):
                $grade="A+";
                break;




            case($mark>69):
                $grade="A";
                break;


            case($mark>59):
                $grade="A-";
                break;


            case($mark>49):
                $grade="B";
                break;


            case($mark>39):
                $grade="c";
                break;


            case($mark>32):
                $grade="D";
                break;

                default: $grade ="f";

        }
        return $grade;
    }

}